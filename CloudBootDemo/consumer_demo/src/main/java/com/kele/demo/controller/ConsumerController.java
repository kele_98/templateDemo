package com.kele.demo.controller;

import com.kele.framework.exception.utils.BadRequestException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.Date;

/**
 * @author chunhui.li
 * @date 2022/9/17 21:37
 * @Description
 * @ClassName ConsumerController
 */
@RestController
@RequestMapping("/consumer")
public class ConsumerController {

    @RequestMapping("/test")
    public String testConsumer(){
        System.out.println("测试gayetway转发路由，时间是：" + System.currentTimeMillis());
        return "测试gayetway转发路由，时间是：" + System.currentTimeMillis();
    }

    @RequestMapping("/testExection")
    public void testExection(){
        throw new BadRequestException("测试全局异常");
    }
}
