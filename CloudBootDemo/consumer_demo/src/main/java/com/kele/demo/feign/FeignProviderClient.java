package com.kele.demo.feign;


import com.kele.utils.RequestResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author chunhui.li
 * @date 2022/2/11 15:48
 * @Description
 * @ClassName FeignProviderClient
 */
@FeignClient(value = "provider-demo")
public interface FeignProviderClient {

    @GetMapping("/provider/getAllUser")
    public RequestResult getAllUser();
}
