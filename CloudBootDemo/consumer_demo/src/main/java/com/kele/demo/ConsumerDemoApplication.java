package com.kele.demo;

import com.kele.utils.BeanUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author chunhui.li
 * @date 2022/2/11 15:08
 * @Description
 * @ClassName ConsumerDemoApplication
 */
@SpringBootApplication
@EnableFeignClients
public class ConsumerDemoApplication {
    public static void main(String[] args) {
        BeanUtil.applicationContext = SpringApplication.run(ConsumerDemoApplication.class, args);
    }
}
