package com.kele.demo.controller;

import com.kele.demo.feign.FeignProviderClient;
import com.kele.utils.RequestResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author chunhui.li
 * @date 2022/2/11 15:54
 * @Description
 * @ClassName FeignProviderController
 */
@RestController
@RequestMapping("/feign")
public class FeignProviderController {

    @Autowired
    FeignProviderClient feignProviderClient;

    @GetMapping("/getAll")
    public RequestResult getAllUser(){
        System.out.println(feignProviderClient.getAllUser());
        return feignProviderClient.getAllUser();
    }
}
