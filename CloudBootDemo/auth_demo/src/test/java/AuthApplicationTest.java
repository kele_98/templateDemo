import com.kele.demo.AuthApplication;
import com.kele.utils.RedisUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author chunhui.li
 * @date 2022/2/14 11:27
 * @Description
 * @ClassName AuthApplicationTest
 */
@SpringBootTest(classes = AuthApplication.class)
public class AuthApplicationTest {

    @Autowired
    RedisUtil redisUtil;

    @Test
    void fun(){
        System.out.println(redisUtil.get("hhh"));
    }
}
