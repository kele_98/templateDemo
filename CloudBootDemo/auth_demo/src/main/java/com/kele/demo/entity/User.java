package com.kele.demo.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author chunhui.li
 * @date 2022/2/14 16:43
 * @Description
 * @ClassName User
 */
@Data
@NoArgsConstructor
public class User {

    private Integer id;
    private String userName;
    private String password;
}
