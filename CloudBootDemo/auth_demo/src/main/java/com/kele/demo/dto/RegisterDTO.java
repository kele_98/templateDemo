package com.kele.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author chunhui.li
 * @date 2022/2/16 14:20
 * @Description
 * @ClassName RegisterDTO
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegisterDTO {

    private String userName;
    private String password;
    private String phone;
    private String email;
    private Integer isSuper;
    private Date createTime;
    private Date updateTime;
}
