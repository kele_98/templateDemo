package com.kele.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kele.demo.entity.LoginUser;

/**
 * @author chunhui.li
 * @date 2022/2/15 14:52
 * @Description
 * @ClassName LoginUserMapper
 */
public interface LoginUserMapper extends BaseMapper<LoginUser> {
}
