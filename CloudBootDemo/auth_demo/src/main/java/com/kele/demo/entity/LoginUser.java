package com.kele.demo.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author chunhui.li
 * @date 2022/2/15 10:08
 * @Description
 * @ClassName LoginUser
 */
@Data
@TableName("tb_login_user")
public class LoginUser {

    @TableField(value = "id")
    @TableId(type = IdType.AUTO)
    private Long id;
    @TableField(value = "name")
    private String userName;
    private String password;
    @TableField(exist = false)
    private String token;
    private String phone;
    private String email;
    private Integer isSuper;
//    @DateTimeFormat(pattern="yyy-MM-dd HH:mm:ss")
//    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Date createTime;
//    @DateTimeFormat(pattern="yyy-MM-dd HH:mm:ss")
//    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
