package com.kele.demo.config;

import com.kele.demo.interceptor.TokenInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author chunhui.li
 * @date 2022/2/14 15:55
 * @Description
 * @ClassName WebAppConfiguration
 */
@Configuration
public class WebAppConfiguration implements WebMvcConfigurer {


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(setTokenInterceptor()).addPathPatterns("/**").excludePathPatterns("/login/**");
    }
    @Bean
    public TokenInterceptor setTokenInterceptor(){
        return new TokenInterceptor();
    }
}
