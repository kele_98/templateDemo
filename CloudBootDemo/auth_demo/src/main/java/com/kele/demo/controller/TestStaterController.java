package com.kele.demo.controller;

import com.kele.demo.annotation.AuthToken;
import com.kele.demo.service.impl.LoginUserServiceImpl;
import com.kele.utils.RequestResult;
import com.kele.utils.RequestResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author chunhui.li
 * @date 2022/2/14 14:46
 * @Description
 * @ClassName TestStaterController
 */
@RestController
@RequestMapping("/auth")
public class TestStaterController {


    @AuthToken
    @RequestMapping("/myStater")
    public RequestResult<String> testStater(){
        return RequestResultUtil.success();
    }
}
