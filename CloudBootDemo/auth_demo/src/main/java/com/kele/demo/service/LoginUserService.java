package com.kele.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kele.demo.dto.RegisterDTO;
import com.kele.demo.entity.LoginUser;

/**
 * @author chunhui.li
 * @date 2022/2/15 16:00
 * @Description
 * @ClassName LoginUserService
 */
public interface LoginUserService extends IService<LoginUser> {
    /**登陆*/
    LoginUser getUser(LoginUser loginUser);
    /**注册*/
    int registerUser(RegisterDTO registerDTO);
}
