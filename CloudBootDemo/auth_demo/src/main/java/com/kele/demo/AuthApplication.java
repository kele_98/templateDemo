package com.kele.demo;


import com.kele.utils.BeanUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Arrays;

/**
 * @author chunhui.li
 * @date 2022/2/14 11:02
 * @Description
 * @ClassName AuthApplication
 */
@SpringBootApplication
@MapperScan("com.kele.demo")
public class AuthApplication {

    public static void main(String[] args) {
        BeanUtil.applicationContext = SpringApplication.run(AuthApplication.class, args);
    }
}
