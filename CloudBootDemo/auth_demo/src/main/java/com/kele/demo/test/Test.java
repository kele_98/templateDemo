package com.kele.demo.test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @author chunhui.li
 * @date 2022/2/17 8:52
 * @Description
 * @ClassName Test
 */
public class Test {

    public Test(){}

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Class<Test> testClass = Test.class;
        Constructor<Test> constructor = testClass.getConstructor(null);
        Test test1 = constructor.newInstance();
        Test test2 = constructor.newInstance();

        System.out.println(test1==test2);
    }
}
