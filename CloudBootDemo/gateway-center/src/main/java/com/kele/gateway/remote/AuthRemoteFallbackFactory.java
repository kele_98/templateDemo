package com.kele.gateway.remote;

import com.kele.framework.exception.utils.BadRequestException;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Service;

/**
 * @author chunhui.li
 * @date 2022/10/9 10:11
 * @Description
 * @ClassName AuthRemoteFallbackFactory
 */
@Service
public class AuthRemoteFallbackFactory implements FallbackFactory<AuthServiceRemote> {
    @Override
    public AuthServiceRemote create(Throwable throwable) {
        return token -> {
            throw new BadRequestException("token校验失败");
        };
    }
}
