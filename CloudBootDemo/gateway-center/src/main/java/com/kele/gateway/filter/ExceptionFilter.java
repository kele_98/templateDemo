package com.kele.gateway.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * @author chunhui.li
 * @date 2022/10/8 13:59
 * @Description
 * @ClassName ExceptionFilter
 */
public class ExceptionFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            chain.doFilter(request, response);
        } catch (Exception e) {
            // 异常捕获，发送到error controller
            request.setAttribute("filter.error", e);
            //将异常分发到/error/exthrow控制器
            request.getRequestDispatcher("/error/exthrow").forward(request, response);
        }
    }
}
