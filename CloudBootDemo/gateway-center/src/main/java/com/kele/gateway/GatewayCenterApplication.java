package com.kele.gateway;


import com.kele.utils.BeanUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author chunhui.li
 * @date 2022/9/17 21:42
 * @Description
 * @ClassName GatewayCenterApplication
 */
@SpringBootApplication
@EnableFeignClients
public class GatewayCenterApplication {

    public static void main(String[] args) {
        BeanUtil.applicationContext=SpringApplication.run(GatewayCenterApplication.class,args);
    }
}
