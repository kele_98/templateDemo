package com.kele.gateway.remote;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author chunhui.li
 * @date 2022/10/9 10:08
 * @Description
 * @ClassName AuthServiceRemote
 */
@FeignClient(name = "auth-demo",path = "/login",fallbackFactory = AuthRemoteFallbackFactory.class)
public interface AuthServiceRemote {

    @GetMapping("/checkToken")
    public Boolean checkToken( @RequestParam(value = "token") String token);
}
