package com.kele.gateway.constant;

/**
 * Created by chuntian.lv on 2018/3/2.
 */
public class Constant {
    public static final int SERVER_ERROR_CODE = 500;
    public static final String SERVER_ERROR_DESC = "服务内部异常";

    public static final String FEIGN_TRUE = "true";
    public static final String FEIGN_FALSE = "false";
}
