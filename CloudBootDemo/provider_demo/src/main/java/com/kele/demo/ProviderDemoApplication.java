package com.kele.demo;

import com.kele.utils.BeanUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author chunhui.li
 * @date 2022/2/11 10:42
 * @Description
 * @ClassName ProviderDemoApplication
 */
@SpringBootApplication
@MapperScan("com.kele.demo.mapper")
@EnableFeignClients
public class ProviderDemoApplication {

    public static void main(String[] args) {
        BeanUtil.applicationContext = SpringApplication.run(ProviderDemoApplication.class, args);
    }
}
