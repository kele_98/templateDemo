package com.kele.demo.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kele.demo.dto.UserInfoDTO;
import com.kele.demo.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kele
 * @since 2022-02-11
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    /**
     * 条件查询返回所有用户信息
     */
    List<User> getAllUserInfo(String name, String email, String address, Page page);

    /**
     * 添加用户
     */
    int insertUserInfo(@Param("userDto") User user);

    /**
     * 修改用户
     */
    int updateUserInfo(@Param("userInfo") UserInfoDTO userInfoDTO);
}
