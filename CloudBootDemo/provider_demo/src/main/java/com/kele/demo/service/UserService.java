package com.kele.demo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kele.demo.dto.UserInfoDTO;
import com.kele.demo.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author kele
 * @since 2022-02-11
 */
public interface UserService extends IService<User> {

    /**
     * 返回所有用户信息
     */
    List<User> getAllUser();

    /**
     * 条件查询返回所有用户信息
     */
    List<User> getAllUserInfo(String userName, String email, String address, Page page);

    /**
     * 添加用户
     */
    int insertUserInfo(UserInfoDTO userInfoDTO);

    /**
     * 修改用户
     */
    int updateUserInfo( UserInfoDTO userInfoDTO);

    /**
     * 删除用户
     */
    int delUserInfo(Long userId);
}
