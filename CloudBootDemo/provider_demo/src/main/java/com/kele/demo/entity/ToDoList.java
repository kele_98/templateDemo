package com.kele.demo.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author chunhui.li
 * @date 2022/3/5 14:55
 * @Description
 * @ClassName ToDoList
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_user_todo_list")
public class ToDoList {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("todo_name")
    private String toDoName;
    private Long userId;
    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 修改时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
