package com.kele.demo.controller;

import com.kele.demo.entity.User;
import com.kele.demo.service.impl.UserServiceImpl;
import com.kele.utils.RequestResult;
import com.kele.utils.RequestResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author chunhui.li
 * @date 2022/2/11 14:42
 * @Description 生产者
 * @ClassName ProviderController
 */
@RestController
@RequestMapping("/provider")
public class ProviderController {

    @Autowired
    UserServiceImpl userService;

    @GetMapping("/getAllUser")
    public RequestResult<User> getAllUser(){
        return RequestResultUtil.success(userService.getAllUser());
    }

    @RequestMapping("/myEnv")
    public RequestResult test(){
        int i =1/0;
        return RequestResultUtil.success();
    }
}
