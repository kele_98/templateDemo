package com.kele.demo.service.impl;

import com.kele.demo.entity.Login;
import com.kele.demo.mapper.LoginMapper;
import com.kele.demo.service.LoginService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kele
 * @since 2022-02-11
 */
@Service
public class LoginServiceImpl extends ServiceImpl<LoginMapper, Login> implements LoginService {

}
