package com.kele.demo.mapper;

import com.kele.demo.entity.Login;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kele
 * @since 2022-02-11
 */
public interface LoginMapper extends BaseMapper<Login> {

}
