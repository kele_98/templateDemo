package com.kele.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kele.demo.entity.ToDoList;

import java.util.List;

/**
 * @author chunhui.li
 * @date 2022/3/5 15:32
 * @Description
 * @ClassName ToDoListService
 */
public interface ToDoListService extends IService<ToDoList> {

    List<ToDoList> getTodoListForUser(Long userId);

    /**
     * 新增待办事项
     */
    Integer addTodoList(ToDoList toDoList);
}
