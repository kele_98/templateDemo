package com.kele.demo.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kele.demo.entity.ToDoList;
import com.kele.demo.mapper.ToDoListMapper;
import com.kele.demo.service.ToDoListService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.logging.Log;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MissingServletRequestParameterException;

import java.util.List;

/**
 * @author chunhui.li
 * @date 2022/3/5 15:33
 * @Description
 * @ClassName ToDoListServiceImpl
 */
@Service
@Slf4j
public class ToDoListServiceImpl extends ServiceImpl<ToDoListMapper, ToDoList> implements ToDoListService {
    @Override
    public List<ToDoList> getTodoListForUser(Long userId) {
        if (userId ==null){
            throw new RuntimeException("没有该用户信息");
        }
        return this.baseMapper.getTodoListForUser(userId);
    }

    @Override
    public Integer addTodoList(ToDoList toDoList) {
        log.info("新增待办事项: {}", JSON.toJSONString(toDoList));
        return this.baseMapper.insert(toDoList);
    }
}
