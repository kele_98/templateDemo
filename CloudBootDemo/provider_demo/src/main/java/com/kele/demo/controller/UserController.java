package com.kele.demo.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kele.demo.dto.UserInfoDTO;
import com.kele.demo.entity.User;
import com.kele.demo.feign.AuthTokenFeign;
import com.kele.demo.service.impl.UserServiceImpl;
import com.kele.utils.RequestResult;
import com.kele.utils.RequestResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author kele
 * @since 2022-02-11
 */
@RestController
@RequestMapping("/user")
@Slf4j
@CrossOrigin(origins = "*")
public class UserController {

    @Autowired
    UserServiceImpl userService;

    @Autowired
    AuthTokenFeign authTokenFeign;

    @GetMapping("/getAllUserInfo")
    public RequestResult getAllUserInfo(@RequestParam(required = false) String name,
                                        @RequestParam(required = false) String email,
                                        @RequestParam(required = false) String address,
                                        @RequestParam Integer pageSize,
                                        @RequestParam Integer pageNum,
                                        HttpServletRequest request){
//        String token = request.getHeader("token");
//        log.info("request : token : {}",token);
//        if (StringUtils.isBlank(token)){
//            return RequestResultUtil.error(403,"登陆过期请重新登陆");
//        } else {
//            Boolean aBoolean = authTokenFeign.checkToken(token);
//            if (aBoolean){
                log.info("入参是：{},{}",pageNum,pageSize);
                Page page =new Page(pageNum,pageSize);
                List<User> allUserInfo = userService.getAllUserInfo(name, email, address, page);
                page.setRecords(allUserInfo);
                return RequestResultUtil.success(page);
//            }
//        }
//        return RequestResultUtil.error(403,"登陆过期请重新登陆");
    }

    @PostMapping("/insert")
    public RequestResult insertUserInfo(@RequestBody UserInfoDTO userInfoDTO,HttpServletRequest request){
        log.info("新增用的入参是: {}",userInfoDTO.toString());
        String token = request.getHeader("token");
        log.info("request : token : {}",token);
        if (StringUtils.isBlank(token)){
            return RequestResultUtil.error(403,"请先登陆");
        } else {
            Boolean aBoolean = authTokenFeign.checkToken(token);
            if (aBoolean) {
                int resullt = userService.insertUserInfo(userInfoDTO);
                if (resullt>0){
                    return RequestResultUtil.success(resullt);
                }else {
                    return RequestResultUtil.error(-1,"新增用户失败");
                }
            } else {
                return RequestResultUtil.error(403,"登陆过期请重新登陆");
            }
        }
    }

    /**
     * 修改用户
     * @param userInfoDTO
     * @param request
     * @return
     */
    @PutMapping("/update")
    public RequestResult updateUserInfo(@RequestBody UserInfoDTO userInfoDTO,HttpServletRequest request){
        log.info("编辑用的入参是: {}",userInfoDTO.toString());
        String token = request.getHeader("token");
        if (StringUtils.isBlank(token)){
            return RequestResultUtil.error(403,"请先登陆");
        } else {
            Boolean aBoolean = authTokenFeign.checkToken(token);
            if (aBoolean) {
                int resullt = userService.updateUserInfo(userInfoDTO);
                if (resullt>0){
                    return RequestResultUtil.success(resullt);
                }else {
                    return RequestResultUtil.error(-1,"修改用户失败");
                }
            } else {
                return RequestResultUtil.error(403,"登陆过期请重新登陆");
            }
        }
    }

    /**
     * 删除用户
     * @param userId
     * @param request
     * @return
     */
    @DeleteMapping("/delete")
    public RequestResult delUserInfo(@RequestParam Long userId,HttpServletRequest request){
        String token = request.getHeader("token");
        if (StringUtils.isBlank(token)){
            return RequestResultUtil.error(403,"请先登陆");
        } else {
            Boolean aBoolean = authTokenFeign.checkToken(token);
            if (aBoolean) {
                int res = userService.delUserInfo(userId);
                if (res>0){
                    return RequestResultUtil.success(res);
                }else {
                    return RequestResultUtil.error(-1,"删除失败");
                }
            }else {
                return RequestResultUtil.error(403,"登陆过期请重新登陆");
            }
        }
    }
}

