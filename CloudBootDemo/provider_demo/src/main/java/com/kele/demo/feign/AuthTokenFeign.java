package com.kele.demo.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author chunhui.li
 * @date 2022/2/19 15:11
 * @Description
 * @ClassName AuthTokenFeign
 */
@FeignClient(value = "auth-demo")
public interface AuthTokenFeign {

    @GetMapping("/login/checkToken")
    public Boolean checkToken( @RequestParam String token);

}
