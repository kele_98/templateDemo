package com.kele.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kele.demo.entity.ToDoList;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author chunhui.li
 * @date 2022/3/5 14:58
 * @Description
 * @ClassName ToDoListMapper
 */
@Mapper
public interface ToDoListMapper extends BaseMapper<ToDoList> {

    List<ToDoList> getTodoListForUser(Long userId);
}
