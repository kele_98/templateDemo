package com.kele.demo.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.databind.BeanProperty;
import com.kele.demo.dto.UserInfoDTO;
import com.kele.demo.entity.User;
import com.kele.demo.mapper.UserMapper;
import com.kele.demo.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kele
 * @since 2022-02-11
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    UserMapper userMapper;

    /**
     * 返回所用用户信息
     * @return
     */
    @Override
    public List<User> getAllUser() {
        return this.baseMapper.selectList(null);
    }

    @Override
    public List<User> getAllUserInfo(String name, String email, String address, Page page) {
        List<User> allUserInfo = userMapper.getAllUserInfo(name, email, address, page);
        return allUserInfo;
    }

    @Override
    public int insertUserInfo(UserInfoDTO userInfoDTO) {
        User user =new User();
        BeanUtils.copyProperties(userInfoDTO,user);
        System.out.println(user.toString());
        return this.baseMapper.insert(user);
    }

    @Override
    public int updateUserInfo(UserInfoDTO userInfoDTO) {
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String format1 = format.format(date);
        userInfoDTO.setUpdateTime(new Date());
        return this.baseMapper.updateUserInfo(userInfoDTO);
    }

    @Override
    public int delUserInfo(Long userId) {
        return this.baseMapper.deleteById(userId);
    }
}
