package com.kele.demo.config;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author chunhui.li
 * @date 2022/2/12 15:13
 * @Description 获得当前微服务的相关信息
 * @ClassName EnvironmentModule
 */
@Data
public class EnvironmentModule {

    private String serviceId;
    private String port;
}
