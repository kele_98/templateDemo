package com.kele.demo.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author kele
 * @since 2022-02-11
 */
@Controller
@RequestMapping("/login")
public class LoginController {

}

