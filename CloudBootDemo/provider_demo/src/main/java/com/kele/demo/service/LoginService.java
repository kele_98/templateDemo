package com.kele.demo.service;

import com.kele.demo.entity.Login;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author kele
 * @since 2022-02-11
 */
public interface LoginService extends IService<Login> {

}
