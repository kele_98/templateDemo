import java.util.Arrays;

/**
 * @author chunhui.li
 * @date 2022/2/25 15:52
 * @Description
 * @ClassName TestDaily
 */
public class TestDaily {

    public static void main(String[] args) {
        int []nums= {1,2,3,4,5};
        int target = 6;
        TestDaily testDaily = new TestDaily();
        System.out.println(testDaily.searchInsert(nums, target));
    }

    public int searchInsert(int[] nums, int target) {
        int low = 0;
        int high = nums.length-1;
        int mid ;
        while (low<high) {
            mid = (high-low)/2+low;
            if (nums[mid]==target) {
                return mid;
            } else if(nums[mid]>target){
                high = mid -1;
            } else {
                low = mid +1;
            }
        }
        return high+1;
    }
}
